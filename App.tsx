import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Footer from './components/Footer';
import Header from './components/Header';
import Present from './Present';
import ForecastPresent from './components/ForecastPresent'
import ForecastPast from './components/ForecastPast'

const App = () => {

  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.main}>
        <ForecastPresent />
        <ForecastPast />
      </View>
      <Footer />
      {/* <Present myState={start} updateState={() => updateState()} />
      <Text>Open up App.tsx to start working on your app!</Text>
      <Text>testing  lorem gdfgd</Text> */}

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#373af5',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  main: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  }
});

export default App;
