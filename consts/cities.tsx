export const CITIES = [
    {
        name: 'Kazan',
        lat: 55.796127,
        lon: 49.106405,
    },
    {
        name: 'Krasnodar',
        lat: 45.03547,
        lon: 38.975313,
    }, {
        name: 'Samara',
        lat: 53.195873,
        lon: 50.100193,
    },
    {
        name: 'Saratov',
        lat: 51.533557,
        lon: 46.034257,
    },
    {
        name: 'Togliatti',
        lat: 53.507836,
        lon: 49.420393,
    },
];
