import React from 'react';
import { StyleSheet, Text, View } from 'react-native';


type Props = {
    title: string
}

const ForecastHeader: React.FC<Props> = ({ title }) => {
    return (
        <View  >
            <Text>
            {title}
            </Text>
          
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        textAlign:'left',
        color: '#2c2d76',
        fontSize: 32
    }

});

export default ForecastHeader;
