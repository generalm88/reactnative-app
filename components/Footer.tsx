import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const Footer: React.FC = () => {
    return (
        <View style={styles.footer}>
            <Text>C ЛЮБОВЬЮ ОТ MERCURY DEVELOPMENT
            </Text>

        </View>
    )
}

const styles = StyleSheet.create({
    footer: {
        fontSize: 16,
        color: '#fff',
        opacity: 0.6,

    }

});

export default Footer;