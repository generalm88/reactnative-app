import React from 'react';
import { StyleSheet, Text, View } from 'react-native';



const Header: React.FC = () => {
    return (
        <View style={styles.container}>

            <Text style={styles.title}>
                <Text >
                    Weather
                    {"\n"}
                    forecast
                </Text>
                {/* <Text style={styles.titleRightPart}>
                        forecast
                    </Text> */}
            </Text>


        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 10.2,
        textAlign: 'left',
        color: '#fff',

    },
    titleRightPart: {

    }

});

export default Header;
