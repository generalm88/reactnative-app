import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ForecastHeader from './ForecastHeader';
import ForecastNextCard from './ForecastNextCard';
import SelectCity from './SelectCity';


const ForecastPresent: React.FC = () => {

    const [context, setContext] = useState<string>();
    const value = {
        context,
        setContext
    }

    return (
        <View style={styles.container}>
            <View >
            <ForecastHeader title='7 Days Forecast' />
            </View>
            <View style={styles.container}>
            <SelectCity />
            </View>
            <ForecastNextCard />

        </View>
        //   <Context.Provider value={value}>
        //     <article className={classes.forecast}>
        //       <section className={classes.forecast__header}>
        //         <ForecastHeader title='7 Days Forecast' />
        //         <div className={classes.select}>
        //           <SelectCity />
        //         </div>
        //       </section>
        //       <ForecastNextCard />
        //     </article>

        //   </Context.Provider>
    )
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      flexDirection: 'column',
      borderRadius: 8
    },
    forecastHeader: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
    }
  });

export default ForecastPresent;