import React, { useContext, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { CITIES } from '../consts/cities';


const SelectCity: React.FC = () => {
    const [selectedValue, setSelectedValue] = useState("Select city");
    // const value = useContext(Context);
    // const citiesList = Object.keys(CITIES);
    // const setSelectCity = (event: React.ChangeEvent<HTMLInputElement>) => {
    //   value.setContext(event.target.value);
    // };
    // const [pickedData, setPickedData] = useState<PickerItem>();

    const CitiesList = CITIES.map(cities => {
        return {
            label: cities.name,
            value: cities.name
        }
        // return <RNPickerSelect.Item key={cities.name} value={cities.name} label={cities.name} />
    });

    const setSelectCity = (pickerItem) => {
        setSelectedValue(pickerItem);
    };


    return (

        <View>
            <RNPickerSelect
                placeholder={{
                    label: 'Select city',
                    value: null,
                }}
                style={{
                    placeholder: {
                        color: '#8083a4',
                        fontSize: 16,
                    },  
                    
    
                    inputAndroid: {
                        fontSize: 16,
                        paddingVertical: 10,
                        paddingHorizontal: 16,
                        borderWidth: 2,
                        borderColor: 'rgba(128,131,164,.2)',
                        borderRadius: 8,
                        color: '#2c2d76',
                        paddingRight: 30,
                        backgroundColor: 'rgba(128,131,164,.06)'
                    },
    
                    // iconContainer: {
                    //     top: 17,
                    //     right: 20,
                    // },
                  
                }}
    
                items={CitiesList}
                onValueChange={(itemValue) => setSelectCity(itemValue)}
            >
                
            </RNPickerSelect>
        </View>


        //   <div className={classes.label__wrapper}>
        //     <label className={classes.form__input}>
        //       <input className={classes.input__city}
        //         type="text"
        //         onChange={(event: React.ChangeEvent<HTMLInputElement>): void => setSelectCity(event)}
        //         placeholder='Select city'
        //         list="lst"
        //       />
        //     </label>


    )
}

export default SelectCity;