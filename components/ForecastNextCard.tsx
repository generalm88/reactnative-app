import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ForecastEmpty from './ForecastEmpty';


type ForecastData = {
    date: number,
    icon: string,
    temp: number
  }
  const ForecastNextCard: React.FC = () => {
    // const value = useContext(Context);
    // const selectCity = value.context;
    const [isForecastGet, setIsForecastGet] = useState(false);
    const [startNumberSlider, setStartNumberSlider] = useState<number>(0);
    const [endNumberSlider, setEndNumberSlider] = useState<number>(3);
    const [numberDaysForecast] = useState<number>(7);
    const [forecastList, setForecastList] = useState<Array<ForecastData>>([])
  
    // useEffect(() => {
    //   if (selectCity && CITIES[selectCity]) {
    //     fetchWeather(CITIES[selectCity].lat, CITIES[selectCity].lon).then((WeeklyForecast) => {
    //       Object.keys(WeeklyForecast.daily).map((key: string, index: number) => {
    //         if (index < numberDaysForecast) {
    //           let dailyForecast: ForecastData = {
    //             date: 0,
    //             icon: '',
    //             temp: 0
    //           };
    //           dailyForecast.date = WeeklyForecast.daily[key].dt * 1000 - (WeeklyForecast.timezone_offset * 1000);
    //           dailyForecast.icon = (`https://openweathermap.org/img/wn/${WeeklyForecast.daily[key].weather[0].icon}.png`)
    //           dailyForecast.temp = (Math.round(WeeklyForecast.daily[key].temp.day));
    //           setForecastList(forecastList => [...forecastList, dailyForecast]);
    //         }
    //         return null;
    //       })
    //       setIsForecastGet(true);
    //     });
    //   }
    //   setIsForecastGet(false);
    // }, [selectCity]);
  
    const getNextWeatherCard = () => {
      if (endNumberSlider + 1 < numberDaysForecast) {
        setStartNumberSlider(startOrder => startOrder + 1);
        setEndNumberSlider(startOrder => startOrder + 1);
      }
    }
  
    const getPrevWeatherCard = () => {
      if (startNumberSlider) {
        setStartNumberSlider(startOrder => startOrder - 1);
        setEndNumberSlider(startOrder => startOrder - 1);
      }
    }
    
    return isForecastGet ? (
        <View>
            <Text>
                forecast
            </Text>
        </View>
    //   <section className={classes.container}>
    //     <div onClick={() => getPrevWeatherCard()} className={`${classes.slider} ${classes.slider__prev} 
    //      ${!startNumberSlider ? classes['slider-disabled'] : ''}`}></div>
    //     {
    //       forecastList.map((weatherCard, index) => {
    //         if (startNumberSlider <= index && index < endNumberSlider)
    //           return <WeatherCard key={index} forecast={weatherCard} />
    //       })
    //     }
    //     <div onClick={() => getNextWeatherCard()} className={`${classes.slider} ${classes.slider__next} 
    //    ${(endNumberSlider + 1 === numberDaysForecast) ? classes['slider-disabled'] : ''}`}></div>
    //   </section >
    ) :
      (
        <ForecastEmpty />
      )
  }
  
  export default ForecastNextCard;
  