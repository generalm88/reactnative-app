import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

const ForecastEmpty: React.FC = () => {

    return (
        <View style={styles.content}>
            <Image style={styles.contentImage}
                source={require('../assets/Placeholder/Academy-Weather-bg160.svg')}
            />
            <Text style={styles.contentMessage}>
                Fill in all the fields and the weather will be displayed
            </Text>
        </View >
    )
}
const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contentMessage: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    contentImage: {
        width: 100,
        height: 100,
    }
});

export default ForecastEmpty;
